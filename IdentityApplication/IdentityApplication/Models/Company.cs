﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace IdentityApplication.Models
{
    public class Company
    {
        [ForeignKey(name:"CompanyId")]
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public bool IsActive { get; set; }
        public List<AppUser> Users { get; set; }

    }
}
