﻿using Microsoft.AspNetCore.Identity;

namespace IdentityApplication.Models
{
    public class AppUser : IdentityUser
    {
        public bool IsActive { get; set; }

        public Company Company { get; set; }
        public string Address { get; set; }

    }
}
