﻿using IdentityApplication.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace IdentityApplication.Data
{
    public class ApplicationDbContext : IdentityDbContext <AppUser>
    {
        public DbSet<Company> Company { get; set; }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
            //Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<AppUser>().Property(e => e.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<Company>().ToTable("Company");
            modelBuilder.Entity<Company>().Property(c => c.CompanyId).ValueGeneratedOnAdd().UseIdentityColumn();
            modelBuilder.Entity<Company>().HasMany(u => u.Users).WithOne(c => c.Company);
           
        }
    }
}
