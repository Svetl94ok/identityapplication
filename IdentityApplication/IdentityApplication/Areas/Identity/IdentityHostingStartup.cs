﻿using Microsoft.AspNetCore.Hosting;



[assembly: HostingStartup(typeof(IdentityApplication.Areas.Identity.IdentityHostingStartup))]
namespace IdentityApplication.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
            });
        }
    }
}